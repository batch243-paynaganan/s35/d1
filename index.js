const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3001;

// [Section] MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@zuittbatch243.e2dwbki.mongodb.net/test", 
	{ 
		useNewUrlParser : true,  
		useUnifiedTopology : true
	}
);
let db = mongoose.connection; 

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

// [Section] Mongoose Schemas
const taskSchema = new mongoose.Schema({ 
	name : String,
	status : { 
		type : String,
		// Default values are the predefined values for a field if we don't put any value
		default : "pending"
	}
})

// [Section] Models
const Task = mongoose.model("Task", taskSchema); 

// [Section] Creation of todo list routes
app.use(express.json());
app.use(express.urlencoded({extended:true})); 

// Creating a new task

// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
	- If the task already exists in the database, we return an error
	- If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/tasks", (req, res)=> {
	Task.findOne({name : req.body.name}, (err, result) => {
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found");
		} else {
			let newTask = new Task({
				name : req.body.name
			});
			newTask.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr);
				} else {
					return res.status(201).send("New task created");
				}
			})
		}
	})
})
app.get("/tasks",(req,res)=>{
    Task.find({},(err,result)=>{
        if(err){
            return console.log(err)
        }else{
            return res.status(200).json({
                data:result
            })
        }
    })
})


app.listen(port, () => console.log(`Server running ${port}`))